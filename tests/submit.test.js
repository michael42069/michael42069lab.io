const expect = require('chai').expect
const process = require('../src/js/logic/submit').process

describe("Submit functionality", () => {
    it("takes request and responds correctly", () => {
        requestObject = {
            word: "hello"
        }
        
        response = process(requestObject);

        console.log("Response: " + response);
        expect(response).to.have.property('isValidWord');
        expect(response).to.have.property('isLettersInWord');
        expect(response.isLettersInWord).to.be.a("array")
    })
})