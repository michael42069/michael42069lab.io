const expect = require('chai').expect
const {isValidWord, isLettersInWord} = require('../src/js/logic/wordCheck')

describe("Validate Functionality", () => {
    it("valid word is classified as such", () => {
        
        word = "hello" //should be in the word file
        
        isValid = isValidWord(word)

        expect(isValid).to.be.true
    })

    it("invalid word is classified as such", () => {
        
        word = "ttttt" //should NOT be in the word file
        
        isValid = isValidWord(word)

        expect(isValid).to.be.false
    })

    it("letters not in the word are identified", () => {
        word = "hello";
        correct = "apple";

        pos = isLettersInWord(word, correct);

        expect(pos[0]).to.be.equal(0);
        expect(pos[1]).to.be.not.equal(0);
        expect(pos[2]).to.be.not.equal(0);
        expect(pos[3]).to.be.not.equal(0);
        expect(pos[4]).to.be.equal(0);
    })

    it("correct letter in the wrong position are identified", () => {
        word = "hello";
        correct = "apple";

        pos = isLettersInWord(word, correct);

        expect(pos[0]).to.be.not.equal(1);
        expect(pos[1]).to.be.equal(1);
        expect(pos[2]).to.be.equal(1);
        expect(pos[3]).to.be.not.equal(1);
        expect(pos[4]).to.be.not.equal(1);
    })

    it("letters in the correct position are identified", () => {
        word = "hello";
        correct = "apple";

        pos = isLettersInWord(word, correct);

        expect(pos[0]).to.be.not.equal(2);
        expect(pos[1]).to.be.not.equal(2);
        expect(pos[2]).to.be.not.equal(2);
        expect(pos[3]).to.be.equal(2);
        expect(pos[4]).to.be.not.equal(2);
    })
})