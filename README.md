# SE2800 Spring 2022 - Michael

### Hello my children

## Setup
When you first pull the project, run `npm i` to install the necessary libraries. Opening `index.html` will show the home page.
#
## For UI development
We will be using [Tailwind CSS](https://tailwindcss.com/) to make UI development easier.  

In order to see uses of the TailwindCSS class library, Tailwind has to build the css file that will be used when changes are made.  

Running `npm run dev` from the top-level repo directory will build the css, and **watch all html and javascript files for changes**. While this is running, just refresh the page in your browser and you'll see the new changes.  

Run `npm run build` to just build the css file from the HTML file without watching for changes.
#
## Testing with ☕Mocha
Make sure you've run `npm i` in the directory. Then, if this is your first time using mocha on your current machine, run `npm i --global mocha` to be able to use mocha in the command line.  

After writing your test, just run `mocha ./path/to/your/test.test.js` to execute it.

To execute all of the tests in the repo, run `npm run test`, and all of the results will show in the console. 
> Make sure to name all tests as `testName.test.js` 
#


<details>
  <summary>Shhhhh, a secret</summary>
  ![Secret](https://i.imgur.com/jHt3mCE_d.webp?maxwidth=760&fidelity=grand) 
</details>