const { isValidWord, isLettersInWord } = require('./wordCheck.js');

module.exports = {
    /**
     * This defines the ins and outs of the program, calling any functions
     * necessary to process the submission of a word
     */
    process: (submitRequest) => {
        //submitRequest object to have word attribute
        word = submitRequest.word
        //TODO:implement checking of word, letters

        correct = "hello" //TODO: implement way to choose and store word of the day
        
        submitResponse = {
            isValidWord: isValidWord(word), //get validity with other logic file
            isLettersInWord: isLettersInWord(word, correct)
        }

        return submitResponse
    },
}